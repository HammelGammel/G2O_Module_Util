// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online Utility Scripts             -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

local lastTick = getTickCount();
local precision = 100;

// ------------------------------------------------------------------- //

addEventHandler("onTick", function()
{
	local tick = getTickCount();
	if (tick > lastTick)
	{
		RealTimeEvent.check();
		lastTick = tick + precision;
	}
});

// ------------------------------------------------------------------- //

class RealTimeEvent
{
	static function remove(tag)
	{
		foreach (key, value in events)
		{
			if (value.tag == tag)
			{
				events.remove(key);
				return true;
			}
		}
		return false;
	}

	// ------------------------------------------------------------------- //

	// -- Events that get triggered at a certain time of day -- //
	// -- Use tag other than "any" to make events unique -- //
	// -- If reschedule == true, restart timer when duplicate -- //
	static function add(date, func, tag = "any", reschedule = false)
	{
		local duplicate = false;
		if (tag != "any")
		{
			foreach (key, event in events)
			{
				if (event.tag == tag)
				{
					duplicate = true;
					if (reschedule)
						events.remove(key);

					break;
				}
			}
		}

		if (!duplicate || reschedule)
		{
			if (events.len() == 0)
			{
				events.append({["date"] = date, ["func"] = func, ["tag"] = tag});
			}
			else
			{
				for (local i = 0; i < events.len(); i++)
				{
					local event = events[i];
					if (Util.isTimeLater(date, event.date) || i == events.len() - 1)
					{
						events.insert(i, {["date"] = date, ["func"] = func, ["tag"] = tag});
						break;
					}
				}
			}
		}
	}

	// ------------------------------------------------------------------- //

	static function check()
	{
		if (events.len() > 0)
		{
			local currDate = date();
			local event = events.top();

			if (Util.isTimeLater(currDate, event.date))
			{
				events.pop();
				event.func();
				check();
			}
		}
	}

	// ------------------------------------------------------------------- //
	// ------------------------------------------------------------------- //

	static events = array(0);
}
