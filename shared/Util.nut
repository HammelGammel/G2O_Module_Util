// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online Utility Scripts             -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class Util
{
	static function getAngleDelta(angle1, angle2)
	{
		local deltaDirect = abs(angle1 - angle2);
		if (deltaDirect <= 180)
			return deltaDirect;
		return 360 - deltaDirect;
	}

	// ------------------------------------------------------------------- //

	static function reverseTable(table)
	{
		local returnTable = {};

		foreach(key, value in table)
			returnTable[value] <- key;

		return returnTable;
	}

	// ------------------------------------------------------------------- //

	static function round(value, decimalPlaces = 0)
	{
		if (decimalPlaces != 0)
		{
			local f = pow(10, decimalPlaces) * 1.0;
			local newValue = value * f;
			newValue = floor(newValue + 0.5);
			newValue = (newValue * 1.0) / f;
			return newValue;
		}

		return floor(value.tofloat() + 0.5);
	}

	// ------------------------------------------------------------------- //

	// -- Returns true, if parameter 2 appears first in parameter 1 -- //
	static function isFirstWord(string, word)
	{
		return string.len() >= word.len()
		&& string.slice(0, word.len()) == word;
	}

	// ------------------------------------------------------------------- //

	static function lerp(from, to, delta, precision = 1.0)
	{
		if (delta > 1)
			delta = 1;

		local newAngle = (1 - delta) * from + delta * to;
		if (abs(to - newAngle) <= precision)
			return to;
		return newAngle;
	}

	// ------------------------------------------------------------------- //

	function lerpAngle(from, to, delta)
	{
		if (from == to)
			return from;

		local directDelta = abs(from - to);
		if (abs(directDelta) <= 180)
			return lerp(from, to, delta);
		else
		{
			local indirectDelta = 360 - directDelta;
			local interpDelta = lerp(0, indirectDelta, delta); //lerp = 16
			if (from < to)
			{
				local angle = from - interpDelta;
				if (angle < 0)
					return angle + 360;
				return angle;
			}
			else
			{
				local angle = from + interpDelta;
				if (angle > 360)
					return angle - 360;
				return angle;
			}
		}
	}

	// ------------------------------------------------------------------- //

	static function moveOnAngle(xParam, yParam, zParam, angle, delta)
	{
		local angleRad = angle * (PI / 180);
		local x = xParam + sin(angleRad) * delta;
		local z = zParam + cos(angleRad) * delta;

		return {["x"] = x, ["y"] = yParam, ["z"] = z};
	}

	// ------------------------------------------------------------------- //

	static function integerToBool(value)
	{
		return value != 0;
	}

	// ------------------------------------------------------------------- //

	static function stringToBool(inString)
	{
		return inString != "false" && inString != "0";
	}

	// ------------------------------------------------------------------- //

	static delimiter = "||";
	static function tableToString(inTable)
	{
		local outString = "t{";
		local firstPair = true;
		foreach (key, value in inTable)
		{
			if (!firstPair)
				outString += delimiter;

			local currString = valueToString(value);
			local currKey = valueToString(key);
			outString += currKey + delimiter + currString;
			firstPair = false;
		}
		outString += "}";
		return outString;
	}

	// ------------------------------------------------------------------- //

	static function stringToTable(inString)
	{
		inString = getEnclosedString(inString);

		local tableOut = {};
		local lastDelimiter = 0;
		while(true)
		{
			local currDelimiter = inString.find(delimiter, lastDelimiter);
			if (currDelimiter == null)
				break;

			local nextDelimiter = getNextDelimiter(inString, currDelimiter + delimiter.len());
			local key = stringToValue(inString.slice(lastDelimiter, currDelimiter));
			local value = stringToValue(inString.slice(currDelimiter + delimiter.len(), nextDelimiter));
			lastDelimiter = nextDelimiter + delimiter.len();
			tableOut[key] <- value;
		}

		return tableOut;
	}

	// ------------------------------------------------------------------- //

	static function getNextDelimiter(inString, startIndex = 0, bracketOpen = "{", bracketClosed = "}")
	{
		local currIndex = startIndex;
		while (true)
		{
			local currBracket = inString.find(bracketOpen, currIndex + 1);
			local currDelimiter = inString.find(delimiter, currIndex + 1);
			if (currDelimiter == null)
				currDelimiter = inString.len();

			if (currDelimiter < currBracket || currBracket == null)
				return currDelimiter;
			else
				currIndex = getClosingBracket(inString, currBracket, bracketOpen, bracketClosed);
		}
	}

	// ------------------------------------------------------------------- //

	static function getClosingBracket(inString, startIndex = 0, bracketOpen = "{", bracketClosed = "}")
	{
		local open = inString.find(bracketOpen, startIndex);
		if (open == null)
			return inString.len();
		local currIndex = open;
		local open = 1;
		local closed = 0;

		while (open > closed)
		{
			local currOpen = inString.find(bracketOpen, currIndex + 1);
			local currClosed = inString.find(bracketClosed, currIndex + 1);
			if (currOpen != null && currOpen < currClosed)
			{
				open++;
				currIndex = currOpen;
			}
			else
			{
				closed++;
				currIndex = currClosed;
			}

			// -- No closing bracket -- //
			if (currClosed == null)
				return inString.len();
		}

		return currIndex;
	}

	// ------------------------------------------------------------------- //

	// -- {test0{test1}} -> test0{test1}
	static function getEnclosedString(inString, startIndex = 0, bracketOpen = "{", bracketClosed = "}")
	{
		if (inString.len())
		local openingBracket = inString.find(bracketOpen);
		if (openingBracket == null)
			openingBracket = 0;

		local closingBracket = getClosingBracket(inString, startIndex, bracketOpen, bracketClosed);
		if (closingBracket == null)
			return inString;

		return inString.slice(openingBracket + 1, closingBracket);
	}

	// ------------------------------------------------------------------- //

	static function valueToString(value)
	{
		local valueType = type(value);

		if (valueType == "integer")
			return "i" + value.tostring();
		else if (valueType == "float")
			return "f" + value.tostring();
		else if (valueType == "bool")
			return "b" + value.tostring();
		else if (valueType == "table")
			return tableToString(value);
		else
			return "s" + value;

		return "null";
	}

	// ------------------------------------------------------------------- //

	static function stringToValue(inString)
	{
		if (inString != null && type(inString) == "string")
		{
			if (inString.tolower() == "null")
				return null;
			if (inString.find("i") == 0)
				return inString.slice(1, inString.len()).tointeger();
			else if (inString.find("f") == 0)
				return inString.slice(1, inString.len()).tofloat();
			else if (inString.find("b") == 0)
				return stringToBool(inString.slice(1, inString.len()));
			else if (inString.find("t{") == 0)
				return stringToTable(inString);
			else if (inString.find("s") == 0)
				return inString.slice(1, inString.len());
		}

		return inString;
	}

	// ------------------------------------------------------------------- //

	static function splitString(string, delimiter)
	{
		local outArray = array(0);
		local lastFind = 0;

		while (lastFind != null)
		{
			local currString = "";
			local nextFind = string.find(delimiter, lastFind);
			if (nextFind == null)
			{
				currString = string.slice(lastFind, string.len());
				lastFind = null;
			}
			else
			{
				currString = string.slice(lastFind, nextFind);
				lastFind = nextFind + delimiter.len();
			}

			if (currString.len() > 0)
				outArray.append(currString);
		}

		return outArray;
	}

	// ------------------------------------------------------------------- //
	// -- Returns the valid date with a given offset to the given date -- //
	static function addToTime(date, dateDelta)
	{
		local newDate = {["day"] = date.day, ["hour"] = date.hour, ["min"] = date.min, ["sec"] = date.sec};

		newDate.day += dateDelta.day;
		newDate.hour += dateDelta.hour;
		newDate.min += dateDelta.min;
		newDate.sec += dateDelta.sec;

		local minuteAdd = floor(newDate.sec / 60);
		newDate.min += minuteAdd;
		newDate.sec -= minuteAdd * 60;
		local hourAdd = floor(newDate.min / 60);
		newDate.hour += hourAdd;
		newDate.min -= hourAdd * 60;
		local dayAdd = floor(newDate.hour / 24);
		newDate.day += dayAdd;
		newDate.hour -= dayAdd * 24;

		return newDate;
	}

	// ------------------------------------------------------------------- //

	static function isTimeLater(date1, date2)
	{
		if (date1.day > date2.day)
			return true;
		else if (date1.day == date2.day)
		{
			if(date1.hour > date2.hour)
				return true;
			else if (date1.hour == date2.hour)
			{
				if (date1.min > date2.min)
					return true;
				else if (date1.min == date2.min)
				{
					if (date1.sec > date2.sec)
						return true;
				}
			}
		}

		return false;
	}

	// ------------------------------------------------------------------- //

	static function getDeltaGameTimeMinutes(startTime, endTime)
	{
		// -- Starttime is earlier - return direct difference -- //
		if (startTime.hour <= endTime.hour && startTime.min <= endTime.min)
			return (endTime.hour - startTime.hour) * 60 + (endTime.min - startTime.min);
		// -- Starttime is later. Return difference to midnight + endTime -- //
		else
			return (24 - startTime.hour + endTime.hour) * 60 + (60 - startTime.min + endTime.min);
	}

	// ------------------------------------------------------------------- //

	function isTimeBetween(startTime, endTime, currentTime)
	{
		local currentLaterThanStartTime = currentTime.hour < endTime.hour   || (currentTime.hour == endTime.hour && currentTime.min <= endTime.min);
		local currentEarlierThanEndTime = currentTime.hour > startTime.hour || (currentTime.hour == startTime.hour && currentTime.min >= startTime.min);

		// -- Endtime is later than starttime -- //
		if (endTime.hour > startTime.hour || (endTime.hour == startTime.hour && endTime.min >= startTime.min))
			// -- CurrentTime is later than starttime and earlier than endtime -- //
			return currentLaterThanStartTime && currentEarlierThanEndTime;
		// -- Starttime is later than endtime -- //
		else
			return currentLaterThanStartTime || currentEarlierThanEndTime;
	}

	// ------------------------------------------------------------------- //

	static function cloneArray(inArray)
	{
		local arrayOut = array(inArray.len());
		foreach (i, value in inArray)
			arrayOut[i] = value;
		return arrayOut;
	}

	// ------------------------------------------------------------------- //

	static function cloneTable(inTable)
	{
		local tableOut = {};
		foreach (key, value in inTable)
			tableOut[key] <- value;
		return tableOut;
	}

	// ------------------------------------------------------------------- //

	static function getNearestPlayer(x, y, z, maxDistance = -1)
	{
		local lastPid = -1;
		local lastDistance = -1;

		for (local pid = 0; pid < getMaxSlots(); pid++)
        {
			if (isPlayerConnected(pid))
			{
	            local pos = getPlayerPosition(pid);
				local distance = getDistance3d(x, y, z, pos.x, pos.y, pos.z);

				if (maxDistance < 0 || (distance <= maxDistance &&
					(distance < lastDistance || lastDistance == -1) ))
				{
					lastPid = pid;
					lastDistance = distance;
				}
			}
        }

		return lastPid;
	}
}
