// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online Utility Scripts             -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class Cell
{
	// -- returns all the not-empty cellindexes within a given radius (in a square: a radius of 1 will return 9 cells, a radius of 2 25, etc.) around a position
	// -- radius in grids -- //
	function getAdjacentCells(x, z, radius)
	{
		local values = {};

		local centerX = (x.tofloat() / GridSize).tointeger();
		local centerZ = (z.tofloat() / GridSize).tointeger();

		local startX = centerX - radius;
		local startZ = centerZ - radius;
		local endX = centerX + radius;
		local endZ = centerZ + radius;

		local currX = startX;
		local currZ = startZ;
		while (currZ <= endZ)
		{
			local currIndex = currX + "," + currZ;
			if (currIndex in Cells)
				foreach (key, value in Cells[currIndex])
					values[key] <- value;

			currX++;
			if (currX > endX)
			{
				currX = startX;
				currZ++;
			}
		}

		return values;
	}

	// ------------------------------------------------------------------- //

	// -- radius in G2 units -- //
	function getContentNear(x, z, radius)
	{
		return getAdjacentCells(x, z, radius / GridSize);
	}

	// ------------------------------------------------------------------- //

	// -- Returns the Cell-Index -- //
	function getIndexByPos(x, z)
	{
		local x = (x.tofloat() / GridSize).tointeger();
		local z = (z.tofloat() / GridSize).tointeger();

		return (x + "," + z);
	}

	// ------------------------------------------------------------------- //

	function addToCellByPos(x, z, indentifier, value)
	{
		local index = getIndexByPos(x, z);
		addToCell(index, indentifier, value);
		return index;
	}

	// ------------------------------------------------------------------- //

	// -- For example "100,600" -- //
	function addToCell(index, indentifier, value)
	{
		index = index.tostring();

		if (!(index in Cells))
			Cells[index] <- {};

		if (!(indentifier in Cells[index]))
			Cells[index][indentifier] <- {};

		Cells[index][indentifier] = value;
		return Cells[index][indentifier];
	}

	// ------------------------------------------------------------------- //

	function removeFromCell(index, indentifier)
	{
		if (index in Cells && indentifier in Cells[index])
		{
			delete Cells[index][indentifier];

			// -- Cell is empty and can be removed -- //
			if (Cells[index].len() == 0)
				clearCell(index);

			return true;
		}

		return false;
	}

	// ------------------------------------------------------------------- //

	function clearCell(index)
	{
		if (index in Cells)
		{
			delete Cells[index];
			return true;
		}
		return false;
	}

	// ------------------------------------------------------------------- //
	// ------------------------------------------------------------------- //

	constructor(gridSize = 500)
	{
		Cells = {};
		GridSize = gridSize;
	}

	// ------------------------------------------------------------------- //
	// ------------------------------------------------------------------- //

	Cells = null;

	// -- Size of a single grid in g2 units -- //
	GridSize = 0;
}
